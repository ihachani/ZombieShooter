﻿using UnityEngine;
using System.Collections;

public class EnemyCreator : MonoBehaviour {

    public float frequency = 2f;
    public Cloner[] enemyPrefabs;
    public Transform zombieTarget;
    public NavAgentHandler[] navAgentHandler;
    public HealthHandler[] healthHandlers;
    public int maxZombiesNumber = 1;

    public GameObject winningView;

    int currentZombiesNumber = 0;

    void Start()
    {
        StartCoroutine(buildEnemy(frequency));
    }

    void Update()
    {
        if (currentZombiesNumber == maxZombiesNumber)
        {
            winningView.SetActive(true);
            Destroy(gameObject);
        }
    }


    IEnumerator buildEnemy(float perdiod)
    {
        while (currentZombiesNumber < maxZombiesNumber)
        {
            currentZombiesNumber += 1;
            GameObject currentEnemy = createEnemy();
            yield return new WaitForSeconds(perdiod);
        }        
    }

    GameObject createEnemy()
    {        
        int selection = (new System.Random()).Next(0, enemyPrefabs.Length);
        Cloner enemyCloner = enemyPrefabs[selection];
        return enemyCloner.clone(gameObject);
    }

}
