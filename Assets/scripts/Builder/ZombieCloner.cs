﻿using UnityEngine;
using System;
using System.Collections;

public class ZombieCloner : Cloner
{

    public int droppableChance = 5;
    public int minDamage = 10;
    public int maxDamage = 10;
    public int minHealth = 1;
    public int maxHealth = 100;
    public int minBonuse = 5;
    public int maxBonuse = 10;

    public override GameObject clone(GameObject parent)
    {
        GameObject clone = base.clone(parent);
        AddHealth(clone);
        AddAttack(clone);
        //AddDroppable(clone);
        //AddTexture(clone);
        spawnZombie();
        clone.SetActive(true);
        AddNavAgentController(clone);
        AddHealthController(clone);
        return clone;
    }

    private void spawnZombie()
    {
        //TODO: spawn zombies
    }

    private void AddHealthController(GameObject clone)
    {
        Health health = clone.GetComponent<Health>();
        //health.healthHandlers = clone.GetComponentInParent<EnemyCreator>().healthHandlers;

        HealthHandler[] tmpArray = new HealthHandler[health.healthHandlers.Length + clone.GetComponentInParent<EnemyCreator>().healthHandlers.Length];
        Array.Copy(clone.GetComponentInParent<EnemyCreator>().healthHandlers, tmpArray, clone.GetComponentInParent<EnemyCreator>().healthHandlers.Length);
        Array.Copy(health.healthHandlers, 0, tmpArray, clone.GetComponentInParent<EnemyCreator>().healthHandlers.Length, health.healthHandlers.Length);
        health.healthHandlers = tmpArray;

        health.activateInScene();
    }

    private void AddNavAgentController(GameObject clone)
    {
        NavAgent navAgent = clone.GetComponent<NavAgent>();
        navAgent.setTarget(clone.GetComponentInParent<EnemyCreator>().zombieTarget);

        NavAgentHandler[] tmpArray = new NavAgentHandler[navAgent.navAgentHandler.Length + clone.GetComponentInParent<EnemyCreator>().navAgentHandler.Length];
        Array.Copy(navAgent.navAgentHandler, tmpArray, navAgent.navAgentHandler.Length);
        Array.Copy(clone.GetComponentInParent<EnemyCreator>().navAgentHandler, 0, tmpArray, navAgent.navAgentHandler.Length, clone.GetComponentInParent<EnemyCreator>().navAgentHandler.Length);
        navAgent.navAgentHandler = tmpArray;

        //navAgent.navAgentHandler + clone.GetComponentInParent<EnemyCreator>().navAgentHandler
        navAgent.activateInScene();
    }

    private void AddAttack(GameObject clone)
    {
        ZombieAttackTargetHandler cloneZombieAttackTargetHandler = clone.GetComponent<ZombieAttackTargetHandler>();
        if (cloneZombieAttackTargetHandler == null) cloneZombieAttackTargetHandler = clone.AddComponent<ZombieAttackTargetHandler>();
        cloneZombieAttackTargetHandler.damage = (new System.Random()).Next(minDamage, maxDamage + 1);
    }

    private static void AddTexture(GameObject clone)
    {
        SkinnedMeshRenderer zombieMesh = clone.GetComponentInChildren<SkinnedMeshRenderer>();
        if (zombieMesh != null)
        {
            zombieMesh.material = Instantiate(Resources.Load("Material/zombie/Zombie_1")) as Material;
        }
    }

    private void AddDroppable(GameObject clone)
    {
        if ((new System.Random()).Next(1, 11) <= droppableChance)
        {
            DroppableProvider droppableProvider = clone.AddComponent<DroppableProvider>();
            droppableProvider.bonus = (new System.Random()).Next(minBonuse, maxBonuse + 1);
            droppableProvider.bonusManager = BonusManager.getCurrentInstance();
            droppableProvider.bonusprefab = (Resources.Load("Droppables/Droppable_1") as GameObject).GetComponent<Bonus>();
        }
    }

    private void AddHealth(GameObject clone)
    {
        Health cloneHealth = clone.GetComponent<Health>();
        if (cloneHealth == null) cloneHealth = clone.AddComponent<Health>();
        cloneHealth.setMax((new System.Random()).Next(minHealth, maxHealth + 1));
        cloneHealth.set(cloneHealth.getMax());
    }

}
