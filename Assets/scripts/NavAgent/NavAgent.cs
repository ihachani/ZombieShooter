﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NavAgent : MonoBehaviour
{

    public Transform target;
    public NavAgentHandler[] navAgentHandler;
    public Image mapImage;

    // Movement Event Handler
    public delegate void OnNavAgentMoveEvent(NavAgent navAgent, float offset);
    public event OnNavAgentMoveEvent OnNavAgentMove;

    // Arrive Event Handler
    public delegate void OnNavAgentArriveEvent(NavAgent navAgent, float offset);
    public event OnNavAgentArriveEvent OnNavAgentArrive;

    // Spawn Mesh Agent Event Handler
    public delegate void OnNavAgentSpawnEvent(NavAgent navAgent);
    public event OnNavAgentSpawnEvent OnNavAgentSpawn;

    // Destination update Event Handler
    public delegate void OnNavAgentDestUpdateEvent(NavAgent navAgent, Transform target);
    public event OnNavAgentDestUpdateEvent OnNavAgentDestUpdate;

    NavMeshAgent navagentSkel;
    public bool autoActivate = false;

    void Start()
    {
        if (autoActivate == true)
            activateInScene();
    }

    public void activateInScene()
    {
        navagentSkel = GetComponent<NavMeshAgent>();
        setTarget(target);
        foreach (NavAgentHandler navAgentin in navAgentHandler)
        {
            navAgentin.attach(this);
        }
        if (OnNavAgentSpawn != null)
        {
            OnNavAgentSpawn(this);
        }
    }

    public void setTarget(Transform target)
    {
        this.target = target;
        if ((navagentSkel != null) && (gameObject.active == true))
            navagentSkel.SetDestination(target.position);
    }

    public void UpdateTarget(Transform target)
    {
        setTarget(target);
        if (OnNavAgentDestUpdate != null)
        {
            OnNavAgentDestUpdate(this, target);
        }
    }

    void FixedUpdate()
    {
        if (!navagentSkel.pathPending)
        {
            if (navagentSkel.remainingDistance <= navagentSkel.stoppingDistance)
            {
                if (!navagentSkel.hasPath || navagentSkel.velocity.sqrMagnitude == 0f)
                {
                    if (OnNavAgentArrive != null)
                    {
                        OnNavAgentArrive(this, navagentSkel.remainingDistance);
                    }
                }
            }
            if (OnNavAgentMove != null)
            {
                OnNavAgentMove(this, navagentSkel.remainingDistance);
            }
        }
    }
}