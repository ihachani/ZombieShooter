﻿using UnityEngine;
using System.Collections;

public class HealthTest : MonoBehaviour {
    Health health;
	// Use this for initialization
	void Awake () {
        health = GetComponent<Health>();
	}
	
	// Update is called once per frame
	void Update () {
        if (health.current > 500)
            health.decrease(10);
        else
            health.increase(500);
	}
}
