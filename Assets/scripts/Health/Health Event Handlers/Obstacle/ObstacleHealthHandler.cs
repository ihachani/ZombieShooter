﻿using UnityEngine;
using System.Collections;

public class ObstacleHealthHandler : HealthHandler {

    public override void attach(Health health) {
        //Debug.Log(gameObject.name + " attaching to " + health.gameObject.name);
        health.OnDeath += OnDeathEventHandler;
        health.OnHealthDown += OnHealthDownEventHandler;
    }

    public override void OnDeathEventHandler(Health health)
    {
        //MoveDown and destroy
        Debug.Log("Obstacle" + gameObject.name+ " Was dead");
        ArrayList attackers = GetComponent<CollisionHandler>().getAttackers();
        foreach (GameObject attacker in attackers) {
            attacker.GetComponent<ZombieAttackTargetHandler>().stopAttack();
        }
        //Destroy only
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 6f, gameObject.transform.position.z);
        //Destroy(gameObject);
    }
    public override void OnHealthDownEventHandler(Health health)
    {
        //Debug.Log("Obstacle Was Hit");
    }
}
