﻿using UnityEngine;
using System.Collections;

public class ScoreAgent : MonoBehaviour {

    public Score score;
    public Score winnngScore;

    public void IncreaseScore(Score score)
    {
        score.Increase(score);
        Debug.Log("score is up to"+score.value);
        if (score.HasReached(winnngScore))
        {
            Debug.Log("is a winner");
        }
    }

    public void DecreaseScore(Score score)
    {
        score.Decrease(score);
    }
} 
