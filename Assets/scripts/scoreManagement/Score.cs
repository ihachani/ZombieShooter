﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

    public int value {get ; set ;}
    public int init = 0;

    public void Start()
    {
        value = init;
    }

    public void Increase(Score score)
    {
        this.value += score.value;
    }

    public void Decrease(Score score)
    {
        this.value -= score.value;
    }

    public bool HasReached(Score score)
    {
        return (score.value >= this.value);
    }
}