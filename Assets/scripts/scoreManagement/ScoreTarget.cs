﻿using UnityEngine;
using System.Collections;

public class ScoreTarget : HealthHandler {

    public ScoreAgent scoreAgent;
    public int scoreValue;

    public virtual void attach(Health health)
    {
        health.OnHealthDown += OnHealthDownEventHandler;
    }

    public virtual void OnHealthDownEventHandler(Health health)
    {
        Score decreaseScore = new Score();
        decreaseScore.Start();
        scoreAgent.DecreaseScore(decreaseScore);
    }
}
