﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
public class Weapon : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
}

