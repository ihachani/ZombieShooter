﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour
{

    public WeaponShoot weaponShoot;
    public Weapon currentWeapon { get; set; }


    public void SelectWeapon(Weapon weapon)
    {
        StartCoroutine(ChangeWeaponCouroutine(currentWeapon, weapon));
        currentWeapon = weapon;
    }


    public IEnumerator ChangeWeaponCouroutine(Weapon weaponToDeactive, Weapon weaponToActive)
    {
        if (!weaponToActive.Equals(weaponToDeactive))
        {
            Debug.Log("ChangeWeaponCouroutine from " + weaponToDeactive + " to " + weaponToActive);
            if (weaponToDeactive.gameObject.activeInHierarchy)
            {
                weaponToDeactive.GetComponent<Animation>().Play("weaponChangeDown");
                AnimationClip animClip = GetClipByIndex(0, weaponToDeactive.GetComponent<Animation>());
                //Debug.Log("clip length : " + animClip.length);
                yield return new WaitForSeconds(animClip.length);
                weaponToDeactive.gameObject.SetActive(false);
                yield return 0;
            }
            if (!weaponToActive.gameObject.activeInHierarchy)
            {
                weaponToActive.GetComponent<Animation>().Play("weaponChangeUp");
                AnimationClip animClip = GetClipByIndex(1, weaponToActive.GetComponent<Animation>());
                weaponToActive.gameObject.SetActive(true);
                //Debug.Log("clip length : " + animClip.length);
                yield return new WaitForSeconds(animClip.length);
            }
            yield return 0;
        }
    }

    //TODO Make a class for this if needed.
    /// <summary>
    /// Get animation clip using index.
    /// </summary>
    /// <param name="index">Clip's index to get.</param>
    /// <returns>Clip</returns>
    AnimationClip GetClipByIndex(int index, Animation anim)
    {
        int i = 0;
        foreach (AnimationState animationState in anim)
        {
            if (i == index)
                return animationState.clip;
            i++;
        }
        return null;
    }
}
