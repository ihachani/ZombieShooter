﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine.UI;

public class RadarAgentHandler : NavAgentHandler
{
    public GameObject referenceObject;
    public float mapScale = 1f;

    public override void attach(NavAgent navAgent)
    {
        navAgent.OnNavAgentSpawn += OnNavAgentSpawnEventHandler;
        navAgent.OnNavAgentArrive += OnNavAgentArriveEventHandler;
        navAgent.OnNavAgentMove += OnNavAgentMoveEventHandler;
        navAgent.OnNavAgentDestUpdate += OnNavAgentDestUpdateEventHandler;
    }

    public void attach(FixedAgent fixedAgent)
    {
        fixedAgent.OnNavAgentSpawn += OnFixedAgentSpawnEventHandler;
        fixedAgent.OnNavAgentMove += OnFixedAgentMoveEventHandler;
    }

    public override void OnNavAgentSpawnEventHandler(NavAgent navAgent)
    {
        createRadarFollower(navAgent);
        //Debug.Log("radarAgentsContainer " + navAgent.name + "return");
    }

    private void createRadarFollower(NavAgent navAgent)
    {
        if (navAgent.mapImage != null)
        {
            GameObject radarAgent = new GameObject(navAgent.name + "__radarFollower");
            radarAgent.transform.SetParent(GetComponent<Transform>(), false);
            Image agentRadarImage = radarAgent.AddComponent<Image>();
            agentRadarImage.sprite = navAgent.mapImage.sprite;
            radarAgent.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
            GetComponent<RadarRegistery>().addNavAgent(navAgent, radarAgent);
        }
    }

    public void OnFixedAgentSpawnEventHandler(FixedAgent fixedAgent)
    {
        if (fixedAgent.mapImage != null)
        {
            GameObject radarAgent = new GameObject(fixedAgent.name + "__fixedRadarFollower");
            radarAgent.transform.SetParent(GetComponent<Transform>(), false);
            //Debug.Log(fixedAgent.name + "__fixedRadarFollower");
            Image agentRadarImage = radarAgent.AddComponent<Image>();
            agentRadarImage.sprite = fixedAgent.mapImage.sprite;
            radarAgent.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
            GetComponent<RadarRegistery>().addFixedAgent(fixedAgent, radarAgent);
        }
    }

    public override void OnNavAgentMoveEventHandler(NavAgent navAgent, float offset)
    {
        GameObject radarAgent = (GameObject)(GetComponent<RadarRegistery>().getNavAgents())[navAgent];
        if (radarAgent != null)
        {
            Vector3 agentPosition = navAgent.gameObject.transform.position;
            Vector3 followerPosition = calculatePosition(agentPosition);
            radarAgent.GetComponent<RectTransform>().position = followerPosition;
        }
        //Debug.Log("Entering zone" + navAgent.name + " | offset" + (offset));
    }

    public void OnFixedAgentMoveEventHandler(FixedAgent fixedAgent)
    {
        GameObject radarAgent = (GameObject)(GetComponent<RadarRegistery>().getNavAgents())[fixedAgent];
        if (radarAgent != null)
        {
            Vector3 agentPosition = fixedAgent.gameObject.transform.position;
            Vector3 followerPosition = calculatePosition(agentPosition);
            radarAgent.GetComponent<RectTransform>().position = followerPosition;
        }
        //Debug.Log("Entering zone" + navAgent.name + " | offset" + (offset));
    }

    Vector3 calculatePosition(Vector3 realPosition)
    {
        float dx = realPosition.x - referenceObject.gameObject.transform.position.x;
        float dz = realPosition.z - referenceObject.gameObject.transform.position.z;
        Vector3 position = new Vector3();
        position.x = dx * mapScale + GetComponent<Transform>().position.x;
        position.y = dz * mapScale + GetComponent<Transform>().position.y;
        return position;
    }


    public override void OnNavAgentArriveEventHandler(NavAgent navAgent, float offset)
    {
        //Debug.Log("" + navAgent.name + "Arrived");
    }

    public override void OnNavAgentDestUpdateEventHandler(NavAgent navAgent, Transform target)
    {
        //Debug.Log("Updating destination" + navAgent.name + " | target" + (target.position));
    }

}
