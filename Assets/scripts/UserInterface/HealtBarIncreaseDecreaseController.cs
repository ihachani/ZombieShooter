﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
class HealtBarIncreaseDecreaseController
{
    RectTransform controlledHealthBarTransform;
    float ratio;
    float maxWidth;
    public HealtBarIncreaseDecreaseController(int maxHealth, RectTransform controlledHealthBarTransform)
    {
        this.controlledHealthBarTransform = controlledHealthBarTransform;
        this.maxWidth = getWidth();
        this.ratio = CalculateRatio(maxHealth);
    }
    private float CalculateRatio(int maxHealth)
    {
        float ratio = maxWidth / maxHealth;
        return ratio;
    }

    public void IncreaseByValue(int value)
    {
        if ( MaxWidthSurpassed(value))
        {
            SetWidthToMax();
        }
        else
        {
            AddValueToWidth(value);
        }
    }
    private bool MaxWidthSurpassed(int value)
    {
        return (getWidth() + ValueToWidth(value)) > maxWidth;
    }
    private void SetWidthToMax()
    {
        controlledHealthBarTransform.sizeDelta = new Vector2(maxWidth, 0) + new Vector2(0, getheight());
    }
    private void AddValueToWidth(int value)
    {
        controlledHealthBarTransform.sizeDelta = new Vector2(ValueToWidth(value), 0) + new Vector2(getWidth(), getheight());
    }


    public void DecreaseByValue(int value)
    {
        if (ZeroWidthSurpassed(value))
        {
            SetWidthToZero();
        }
        else
        {
            SubstractValueFromWidth(value);
        }
    }

    private bool ZeroWidthSurpassed(int value)
    {
        return (getWidth() - ValueToWidth(value)) < 0;
    }

    private void SetWidthToZero()
    {
        controlledHealthBarTransform.sizeDelta = new Vector2(0, getheight());
    }

    private void SubstractValueFromWidth(int value)
    {
        controlledHealthBarTransform.sizeDelta = new Vector2(getWidth(), getheight()) - new Vector2(ValueToWidth(value), 0);
    }

    private float ValueToWidth(int value)
    {
        float calculatedValue = value * ratio;
        return calculatedValue;
    }



    private float getWidth()
    {
        return controlledHealthBarTransform.sizeDelta.x;
    }
    private float getheight()
    {
        return controlledHealthBarTransform.sizeDelta.y;
    }
}

