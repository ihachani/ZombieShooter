﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBarNew : HealthHandler
{
    HealtBarIncreaseDecreaseController healthBarController;
    int MAXHEALTH = 100;
    private int lastHealthValue;
    // Use this for initialization
    void Start()
    {
        healthBarController = new HealtBarIncreaseDecreaseController(MAXHEALTH, GetComponent<RectTransform>());
        lastHealthValue = MAXHEALTH;


    }
    public override void attach(Health health)
    {
        health.OnHealthUp += OnHealthUpEventHandler;
        health.OnHealthDown += OnHealthDownEventHandler;
        MAXHEALTH = health.maxHealth;
    }

    public override void OnHealthUpEventHandler(Health health)
    {
        int healthUpValue = ComputeHealthUpValue(health);
        IncreaseHealthBar(healthUpValue);
        UpdateLastHealthValue(health);
    }

    private void UpdateLastHealthValue(Health health)
    {
        lastHealthValue = health.current;
    }

    private int ComputeHealthUpValue(Health health)
    {
        return health.current - lastHealthValue;
    }
    private void IncreaseHealthBar(int value)
    {
        healthBarController.IncreaseByValue(value);
    }

    public override void OnHealthDownEventHandler(Health health)
    {
        int healthDownValue = ComputeHealthDownValue(health);
        DecreaseHealthBar(healthDownValue);
        UpdateLastHealthValue(health);
    }

    private int ComputeHealthDownValue(Health health)
    {
        int healthDownValue;
        healthDownValue = lastHealthValue - health.current;
        return healthDownValue;
    }

    private void DecreaseHealthBar(int value)
    {
        healthBarController.DecreaseByValue(value);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
