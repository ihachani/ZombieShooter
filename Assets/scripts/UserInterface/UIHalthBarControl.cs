﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIHalthBarControl : MonoBehaviour {

    public GameObject healthBarGameObjectTransform;
    public GameObject healthValueGameObjectTransform;

    void Start()
    {
        //Transform healthBar = GetComponent<Transform>().FindChild(healthBarGameObjectTransform.name);
        updateControlUI(healthBarGameObjectTransform);
        //Transform healthValue = GetComponent<Transform>().FindChild(healthValueGameObjectTransform.name);
        //Debug.Log("health value" + (healthValue.gameObject == null));
        updateControlUI(healthValueGameObjectTransform);
    }

    protected GameObject updateControlUI(GameObject gameObject)
    {
        Vector2 offsetMax = new Vector2(0f, 0f);
        Vector2 offsetMin = new Vector2(0, 10f);
        Vector2 anchorMax = new Vector2(1f, 1f);
        Vector2 anchorMin = new Vector2(0f, 0f);
        gameObject.GetComponent<RectTransform>().offsetMax = offsetMax;
        gameObject.GetComponent<RectTransform>().offsetMin = offsetMin;
        gameObject.GetComponent<RectTransform>().anchorMax = anchorMax;
        gameObject.GetComponent<RectTransform>().anchorMin = anchorMin;
        return gameObject;
    }

    void Update()
    {

    }
}
