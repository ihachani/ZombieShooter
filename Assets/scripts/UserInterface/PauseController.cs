﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseController : MonoBehaviour {

    Button resumeButton;
    Button quitButton;

    void Start()
    {        
        GameObject resumeGameObject = transform.FindChild("pauseMenuCanvas").FindChild("resumeButton").gameObject;
        resumeButton = resumeGameObject.GetComponent<Button>();

        GameObject quitGameObject = transform.FindChild("pauseMenuCanvas").FindChild("quitButton").gameObject;
        quitButton = quitGameObject.GetComponent<Button>();

        resumeButton.onClick.AddListener(() => { resumeGame(); });
        resumeButton.onClick.AddListener(() => { quitGame(); });
    }

    private void quitGame()
    {
        //Debug.Log("Quit the game");
        Application.Quit();
    }

    private void resumeGame()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void show()
    {
        Time.timeScale = 0;
        gameObject.SetActive(true);
    }
}
