﻿using UnityEngine;
using System.Collections;

public class HealthUpdater : HealthHandler
{

    public override void attach(Health health)
    {
        health.OnHealthUp += OnHealthUpEventHandler;
        health.OnHealthDown += OnHealthDownEventHandler;
        SetHealthValueTo(health);
    }

    public override void OnHealthUpEventHandler(Health health)
    {
        SetHealthValueTo(health);
    }

    public override void OnHealthDownEventHandler(Health health)
    {
        SetHealthValueTo(health);
    }

    public void SetHealthValueTo(Health health)
    {
        float min = (float)health.get() / health.getMax();
        Vector2 anchorMin = new Vector2(1-min, 0f);
        //Debug.Log("anchorMin First" + GetComponent<RectTransform>().anchorMin);
        gameObject.GetComponent<RectTransform>().anchorMin = anchorMin;
        Vector2 offsetMax = new Vector2(0f, 0f);
        Vector2 offsetMin = new Vector2(0f, 0f);
        //GetComponent<RectTransform>().offsetMax = offsetMax;
        //GetComponent<RectTransform>().offsetMin = offsetMin;
        //Debug.Log("anchorMin" + anchorMin);
    }

}
