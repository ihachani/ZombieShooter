﻿using UnityEngine;
using System.Collections.Generic;

public class LevelSystem : MonoBehaviour {

	GameObject[] levels;
	public int layer = 12;
	public GameObject prefab;
	public int levelNumbre = 15;


	void Awake () {
		levels = FindGameObjectsWithLayer (layer);
		foreach(GameObject obj in levels) {
			for (int y = 0; y < levelNumbre; y++) {
				GameObject newObj = (GameObject)Instantiate(prefab, obj.transform.position, Quaternion.identity);
				newObj.name = y + "";
				newObj.transform.parent = obj.transform;
			}
		}
	}

	GameObject[] FindGameObjectsWithLayer (int layer) {
		GameObject[] goArray = (GameObject[]) GameObject.FindObjectsOfType (typeof(GameObject));
		List<GameObject> goList = new List<GameObject>();
		for (int i = 0; i < goArray.Length; i++) {
			if (goArray[i].layer == layer) {
				goList.Add(goArray[i]);
			}
		}
		if (goList.Count == 0) {
			return null;
		}
		return goList.ToArray();
	}

}
