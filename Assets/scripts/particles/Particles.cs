﻿using UnityEngine;
using System.Collections;

public class Particles : MonoBehaviour {

    public static void startParticleSystem(ParticleSystem system) {
        system.Play();
    }

    public static void stopParticleSystem(ParticleSystem system) {
        system.Stop();
    }
    public static void startParticleSystemWithNewPosistion(ParticleSystem system, Vector3 position) {
        system.transform.position = position;
        system.Play();
    }
}
