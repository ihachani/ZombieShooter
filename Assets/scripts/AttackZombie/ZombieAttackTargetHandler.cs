﻿using UnityEngine;
using System.Collections;

public class ZombieAttackTargetHandler : MonoBehaviour
{
    Ray ray;
    public int damage;
    Animator animator;
    NavMeshAgent agent;

    // Use this for initialization
    void Awake()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();

    }
    // Update is called once per frame
    void Update()
    {
        /*ray = new Ray(transform.position + new Vector3(0f,-0.7f,0f), transform.forward);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 0.5f, 9))
        {
            Debug.Log("Collided");
        }*/
    }
    public IEnumerator attack(GameObject obstacle)
    {

        //animator.SetTrigger("Attack");
        //obstacle.GetComponent<Health>().decrease(damage);
        //agent.Resume();
        while (obstacle != null)
        {
            //Debug.Log("agent.velocity.magnitude for " +gameObject.name+ " is "+ agent.velocity.magnitude);
            if (agent.velocity.magnitude == 0f)
                obstacle.GetComponent<Health>().decrease(damage);
            if (obstacle.GetComponent<Health>().isDead())
            {
                stopAttack();
                yield return null;
            }
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(1).length);
        }
    }
    public void stopAttack()
    {
        StartCoroutine("waitAttackAnimationLayerAndResumeAgent");

    }
    public IEnumerator waitAttackAnimationLayerAndResumeAgent()
    {
        //agent.Stop();
        //while (animator.GetCurrentAnimatorStateInfo(1).normalizedTime < 0.9f)
        //{
        //    yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(1).length);
        //}
        agent.Resume();
        yield return null;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9)
        {
            GameObject obstacle = collision.gameObject;
            //obstacle.GetComponent<Health>().increase(damage * 7);
            //Debug.Log("is It null" + (obstacle.GetComponent<CollisionHandler>() == null) + "__" + gameObject.name);
            //Debug.Log("attacking " + obstacle.name);
            if (obstacle.GetComponent<CollisionHandler>() != null)
                obstacle.GetComponent<CollisionHandler>().add(gameObject);
            agent.Stop();
            StartCoroutine("attack", obstacle);
            //attack(obstacle);
        }
    }
}
